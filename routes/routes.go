package routes

import (
	"github.com/aanatyrnal/cjvfx/handlers"
	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	router := gin.Default()

	router.GET("/task-one", handlers.CalculateDifference)
	router.GET("/task-two", handlers.AnagramCheck)
	router.GET("/task-tree", handlers.StringInt)
	router.GET("/tasks/:file", handlers.GiveFile)

	return router
}
