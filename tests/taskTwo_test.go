package tests

import (
	"github.com/aanatyrnal/cjvfx/models"
	"github.com/aanatyrnal/cjvfx/services"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAnagramCheck(t *testing.T) {
	testCases := []struct {
		strings  models.Strings
		expected models.Response
	}{
		{strings: models.Strings{StringOne: "string_one", StringTwo: "string_two"}, expected: models.Response{Check: false}},
		{strings: models.Strings{StringOne: "anagram", StringTwo: "nagrama"}, expected: models.Response{Check: true}},
		{strings: models.Strings{StringOne: "car", StringTwo: "rat"}, expected: models.Response{Check: false}},
		{strings: models.Strings{StringOne: "expected", StringTwo: "pectedex"}, expected: models.Response{Check: true}},
		{strings: models.Strings{StringOne: "qwerty", StringTwo: "rtyqwa"}, expected: models.Response{Check: false}},
	}

	for _, testCase := range testCases {
		actual := services.AnagramCheck(testCase.strings)
		assert.Equal(t, testCase.expected, actual)
	}
}
