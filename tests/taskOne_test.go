package tests

import (
	"github.com/aanatyrnal/cjvfx/models"
	"github.com/aanatyrnal/cjvfx/services"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCalculateDifference(t *testing.T) {
	testCases := []struct {
		numbers  []int
		expected models.Dif
	}{
		{numbers: []int{7, 1, 5, 3, 6, 4}, expected: models.Dif{Dif: 6}},
		{numbers: []int{7, 6, 4, 3, 1}, expected: models.Dif{Dif: 0}},
		{numbers: []int{3, 3, 3, 3, 3}, expected: models.Dif{Dif: 0}},
		{numbers: []int{9, 5, 2, 1}, expected: models.Dif{Dif: 0}},
		{numbers: []int{9, 5, 2, 7, 1}, expected: models.Dif{Dif: 8}},
	}

	for _, testCase := range testCases {
		actual := services.CalculateDifference(testCase.numbers)
		assert.Equal(t, testCase.expected, actual)
	}
}

func TestCheckDesc(t *testing.T) {
	testCases := []struct {
		numbers  []int
		expected bool
	}{
		{numbers: []int{7, 1, 5, 3, 6, 4}, expected: false},
		{numbers: []int{7, 6, 4, 3, 1}, expected: true},
		{numbers: []int{3, 3, 3, 3, 3}, expected: true},
		{numbers: []int{9, 5, 2, 1}, expected: true},
	}

	for _, testCase := range testCases {
		actual := services.CheckDesc(testCase.numbers)
		assert.Equal(t, testCase.expected, actual)
	}
}
