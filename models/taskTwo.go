package models

type Strings struct {
	StringOne string `json:"s1"`
	StringTwo string `json:"s2"`
}

type Response struct {
	Check bool `json:"check"`
}
