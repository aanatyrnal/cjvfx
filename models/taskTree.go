package models

type StringInt struct {
	String string `json:"string"`
	Num    int    `json:"num"`
}
