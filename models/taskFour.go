package models

type GiveFilesR struct {
	Files []string `json:"files"`
	Error error    `json:"error"`
}

type GiveFileR struct {
	Data  []byte
	Error error
}
