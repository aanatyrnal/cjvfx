package models

type Numbers struct {
	Numbers []int `json:"numbers"`
}

type Dif struct {
	Dif int `json:"difference"`
}
