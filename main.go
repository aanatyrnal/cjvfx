package main

import (
	"fmt"
	"github.com/aanatyrnal/cjvfx/routes"
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

type Config struct {
	Port int `yaml:"port"`
}

func main() {
	configData, err := ioutil.ReadFile("config.yml")
	if err != nil {
		panic(fmt.Errorf("failed to read config file: %w", err))
	}

	var config Config
	err = yaml.Unmarshal(configData, &config)
	if err != nil {
		panic(fmt.Errorf("failed to unmarshal config data: %w", err))
	}

	r := routes.SetupRouter()

	r.Run(fmt.Sprintf(":%d", config.Port))
}
