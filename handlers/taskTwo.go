package handlers

import (
	"github.com/aanatyrnal/cjvfx/models"
	"github.com/aanatyrnal/cjvfx/services"
	"github.com/gin-gonic/gin"
)

func AnagramCheck(c *gin.Context) {
	var request models.Strings
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{"error": "Invalid input"})
		return
	}
	responce := services.AnagramCheck(request)

	c.JSON(200, responce)
}
