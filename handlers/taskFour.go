package handlers

import (
	"github.com/aanatyrnal/cjvfx/services"
	"github.com/gin-gonic/gin"
)

func GiveFile(c *gin.Context) {
	fileName := c.Param("file")
	response := services.GiveFile(fileName)
	if response.Error != nil {
		c.JSON(500, gin.H{"error": response.Error.Error()})
		return
	}

	c.Data(200, "application/octet-stream", response.Data)
}
