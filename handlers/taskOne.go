package handlers

import (
	"github.com/aanatyrnal/cjvfx/models"
	"github.com/aanatyrnal/cjvfx/services"
	"github.com/gin-gonic/gin"
)

func CalculateDifference(c *gin.Context) {
	var request models.Numbers
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{"error": "Invalid input"})
		return
	}

	response := services.CalculateDifference(request.Numbers)

	c.JSON(200, response)
}
