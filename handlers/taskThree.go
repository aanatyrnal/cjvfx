package handlers

import (
	"github.com/aanatyrnal/cjvfx/models"
	"github.com/aanatyrnal/cjvfx/services"
	"github.com/gin-gonic/gin"
	"net/http"
)

func StringInt(c *gin.Context) {
	var request models.StringInt
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid input"})
		return
	}

	response := services.StringInt(request)

	c.JSON(http.StatusOK, gin.H{"response": response})
}
