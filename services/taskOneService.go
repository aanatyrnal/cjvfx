package services

import (
	"github.com/aanatyrnal/cjvfx/models"
	"sort"
)

func CheckDesc(numbers []int) bool {
	for i := 0; i < len(numbers)-1; i++ {
		if numbers[i] < numbers[i+1] {
			return false
		}
	}
	return true
}

func CalculateDifference(numbers []int) models.Dif {
	if len(numbers) == 0 {
		return models.Dif{Dif: 0}
	}

	sortedNumbers := make([]int, len(numbers))
	copy(sortedNumbers, numbers)

	sort.Ints(sortedNumbers)

	min := sortedNumbers[0]
	max := sortedNumbers[len(sortedNumbers)-1]

	if min == max || CheckDesc(numbers) {
		return models.Dif{Dif: 0}
	}

	return models.Dif{Dif: max - min}
}
