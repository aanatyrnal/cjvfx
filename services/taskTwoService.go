package services

import (
	"github.com/aanatyrnal/cjvfx/models"
	"sort"
	"strings"
)

func AnagramCheck(lines models.Strings) models.Response {
	s1 := strings.ToLower(lines.StringOne)
	s2 := strings.ToLower(lines.StringTwo)

	sByt1 := []byte(s1)
	sByt2 := []byte(s2)

	sort.Slice(sByt1, func(i, j int) bool {
		return sByt1[i] < sByt1[j]
	})

	sort.Slice(sByt2, func(i, j int) bool {
		return sByt2[i] < sByt2[j]
	})

	return models.Response{
		Check: string(sByt1) == string(sByt2),
	}
}
