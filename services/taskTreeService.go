package services

import "github.com/aanatyrnal/cjvfx/models"

func StringInt(stringInt models.StringInt) string {
	if stringInt.Num <= 1 || stringInt.Num >= len(stringInt.String) {
		return stringInt.String
	}

	result := make([]rune, 0)
	n := len(stringInt.String)
	cycleLen := 2*stringInt.Num - 2

	for i := 0; i < stringInt.Num; i++ {
		for j := 0; j+i < n; j += cycleLen {
			result = append(result, rune(stringInt.String[j+i]))
			if i != 0 && i != stringInt.Num-1 && j+cycleLen-i < n {
				result = append(result, rune(stringInt.String[j+cycleLen-i]))
			}
		}
	}

	return string(result)
}
