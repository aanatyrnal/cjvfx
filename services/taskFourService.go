package services

import (
	"github.com/aanatyrnal/cjvfx/models"
	"io/ioutil"
	"path/filepath"
)

func GiveFile(filename string) models.GiveFileR {
	filePath := filepath.Join("static", filename)
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return models.GiveFileR{Error: err}
	}

	return models.GiveFileR{Data: data}
}
